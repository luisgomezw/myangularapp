export class Book {
	id: number;
	title: string;
	author: string;
	publishedDate: Date;
	price: number;
	available: boolean;
}

export const DataBooks: Book[] = [
	{ id: 1, title: 'Creativity Inc', author: 'Ed Catmull', publishedDate: new Date(), price: 130, available: true},
	{ id: 2, title: 'Maestria', author: 'Robert Greene', publishedDate: new Date(), price: 180, available: true},
	{ id: 3, title: 'El elemento', author: 'Ken Robinson', publishedDate: new Date(), price: 160, available: true},
	{ id: 4, title: 'Modelo Virgin', author: 'Richard Branson', publishedDate: new Date(), price: 190, available: true},
	{ id: 5, title: 'Lean Startup', author: 'Eric Ries', publishedDate: new Date(), price: 230, available: true}
];