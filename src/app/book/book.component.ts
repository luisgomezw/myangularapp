import { Component, OnInit } from '@angular/core';
import { Book, DataBooks } from './book';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
	// book: Book = {
	// 	title: 'El principito',
	// 	author: 'Mike Nieva',
	// 	publishedDate: new Date(),
	// 	price: 145,
	// 	available: true
	// };

	books = DataBooks;
	selectedBook: Book;
	showForm: boolean;

	constructor() { 
	  	// this.selectedBook;
	  	this.showForm = false;
	}

	ngOnInit() {

	}

  	// Función al seleccionar un libro de la lista.
  	onSelect(book: Book): void{
  		this.selectedBook = book;
  		this.showForm = true;
  	}

  	addBook(): void {
  		this.selectedBook = new Book();
  		this.showForm = true;
  	}

  	// onSaveBook(book: Book): void {
  		
  	// 	if(book.id){
  	// 		// Aquí colocamos el consumo del webservice que actualiza el registro del libro
  	// 		for(let x=0; x<this.books.length; x++){
	  // 			if(this.books[x].id===book.id){
	  // 				this.books[x] = book;
	  // 			}
	  // 		}	
  	// 	} else {
  	// 		// Aquí colocamos el consumo del webservice que almacena el registro del libro
  	// 		this.books.push(book);
  	// 	}
  		
  	// 	this.showForm = false;
  	// }

  	/*
		POST
		PUT
		GET
		DELETE
  	*/

  	onSaveBook(book: Book):void {
  		if(book.id){ //  Actualización de registro
  			for(let x=0; x<this.books.length; x++) {
  				if(this.books[x].id === book.id){
  					// Book.$put(book);
  					this.books[x] = book;
  				}
  			}
  		} else { // Nuevo registro
			// REST-API
			// Book.$post(book);
	  		this.books.push(book);
  		}
  		
  		this.showForm = false;
  	}

}
